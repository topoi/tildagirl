module TG.Logic.Physics where


import Physics.Hipmunk as H hiding (scale)
import Data.StateVar
import Physics.Hipmunk.Utils

import TG.Types

import Control.Monad
import Control.Lens
import Data.Foldable

import Linear

initSimu :: IO Space
initSimu = do
  space <- initSpace

  ground <- addBody space infinity infinity (V2 0 0)

  let plain = lineSegment (V2 (- 50) 0) (V2 50 0) 0
  let plain2 = lineSegment (V2 0 (- 50)) (V2 0 50) 0
  plD <- addShape space ground plain 0 1 (V2 0 (-50))
  plU <- addShape space ground plain 0 1 (V2 0 50)
  plL <- addShape space ground plain2 0 1 (V2 (-50) 0)
  plR <- addShape space ground plain2 0 1 (V2 50 0)

  return space

createSimpleBody :: Space -> V2 Double -> ShapeType -> IO Body
createSimpleBody space pos sh = do
  b <- addBody space 1 1 pos
  addShape space b sh 0 0.8 (V2 0 0)
  return b

initPlayer :: Space -> V2 Double -> IO Body
initPlayer s loc = createSimpleBody s loc (circle 5)

engine :: Body -> Double -> Buff -> IO ()
engine b t o = addImpulse b (enginePower o ^* t) (V2 0 0)

enginePower :: Buff -> V2 Double
enginePower b =
  case b of
    MovingUp -> V2 0 10
    MovingDown -> V2 0 (-10)
    MovingLeft -> V2 (-10) 0
    MovingRight -> V2 10 0

airResist :: Body -> Double -> IO ()
airResist b t = do
  v <- getVelocity b
  let ff = - 0.1 * norm v *^ v
  addImpulse b (ff ^* t) 0

chase :: Body -> Body -> Double -> IO ()
chase b target t = do
  bPos <- getPosition b
  tPos <- getPosition target
  let ff = 0.1 * t
  addImpulse b (ff *^ (tPos - bPos)) 0
