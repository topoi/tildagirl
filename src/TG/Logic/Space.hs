module TG.Logic.Space where

import Control.Monad
import Data.Monoid
import qualified Data.Map as M
import qualified Data.Bimap as BM

import Linear

import Control.Distributed.Process
import Control.Concurrent.MVar

import Physics.Hipmunk
import Physics.Hipmunk.Utils

import TG.Logic.Physics
import TG.Types
import TG.Logic.Misc
import TG.Logic.Player

newGameSpace :: ProcessId -> ProcessId -> Process ()
newGameSpace renderHub controlHub = do

  space <- liftIO $ initGameSpace
  processIndex <- liftIO $ newMVar M.empty
  bodyIndex <- liftIO $ newMVar BM.empty

  self <- getSelfPid
  physicsTimerId <- physicsTimer
  renderTimerId <- renderTimer
  send physicsTimerId self
  send renderTimerId self

  forever $ receiveWait
    [ match $ updateHandler space
    , match $ renderHandler
    , match $ createHandler processIndex bodyIndex self space renderHub controlHub
    ]

renderHandler :: GameRender -> Process ()
renderHandler _ = void $ spawnLocal $ void $ do
  return ()

updateHandler :: Space -> PhysicsUpdate -> Process ()
updateHandler space _ = void $ spawnLocal $ liftIO $ do
  let t = 0.01
  step space t

createHandler
  :: MVar (M.Map PlayerPosition ProcessId)
  -> MVar (BM.Bimap ProcessId Body)
  -> ProcessId
  -> Space
  -> ProcessId
  -> ProcessId
  -> CreateConfig -> Process ()
createHandler processIndex bodyIndex spaceHandle space renderHub controlHub c = void $ spawnLocal $ do
  case c of
    CreatePlayer ppos initState ->  do
      playerBody <- case ppos of
        Player1 -> liftIO $ initPlayer space (V2 0 (-25))
        Player2 -> liftIO $ initPlayer space (V2 0 25)
      playerProcess <- spawnLocal $ newPlayer controlHub renderHub ppos initState (spaceHandle, playerBody)
      liftIO $ modifyMVar_ processIndex (pure . M.insert ppos playerProcess)
      liftIO $ modifyMVar_ bodyIndex (pure . BM.insert playerProcess playerBody)
    CreateBullet pos ppos -> do
      pI <- liftIO $ readMVar processIndex
      bI <- liftIO $ readMVar bodyIndex
      let Just targetPId = M.lookup ppos pI
      target <- BM.lookup targetPId bI
      bulletBody <- liftIO $ createSimpleBody space pos (circle 2)
      bulletProcess <- spawnLocal $ newBullet bulletBody target renderHub
      -- liftIO $ modifyMVar_ processIndex (pure . M.insert ppos playerProcess)
      liftIO $ modifyMVar_ bodyIndex (pure . BM.insert bulletProcess bulletBody)

newBullet :: Body -> Body -> ProcessId -> Process ()
newBullet b target renderHub = do
  self <- getSelfPid
  physicsTimerId <- physicsTimer
  renderTimerId <- renderTimer
  send physicsTimerId self
  send renderTimerId self

  forever $ receiveWait
    [ match $ bulletUpdateHandler b target
    , match $ bulletRenderHandler self b renderHub
    ]

bulletUpdateHandler :: Body -> Body -> PhysicsUpdate -> Process ()
bulletUpdateHandler b target _ = do
  let t = 0.01
  liftIO $ chase b target t

bulletRenderHandler :: ProcessId -> Body -> ProcessId -> GameRender -> Process ()
bulletRenderHandler self b renderHub _ = do
  bPos <- liftIO $ getPosition b
  send renderHub $ BulletRender self bPos

initGameSpace :: IO Space
initGameSpace = do
  space <- initSpace

  ground <- addBody space infinity infinity (V2 0 0)

  let plain = lineSegment (V2 (- 50) 0) (V2 50 0) 0
  let plain2 = lineSegment (V2 0 (- 50)) (V2 0 50) 0
  plD <- addShape space ground plain 0 1 (V2 0 (-50))
  plU <- addShape space ground plain 0 1 (V2 0 50)
  plL <- addShape space ground plain2 0 1 (V2 (-50) 0)
  plR <- addShape space ground plain2 0 1 (V2 50 0)

  return space
