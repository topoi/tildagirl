module TG.Logic.Player where

import Control.Lens
import Control.Monad
import Data.Maybe
import Data.List
import Data.Foldable
import qualified Data.Map as M
import Linear

import Control.Distributed.Process
-- import Control.Distributed.Process.Internal.StrictMVar
import Control.Concurrent.MVar

import Physics.Hipmunk
import Physics.Hipmunk.Utils

import TG.Logic.Physics
import TG.Types
import TG.Logic.Misc

newPlayer :: ProcessId -> ProcessId -> PlayerPosition -> PlayerState -> (ProcessId, Body) -> Process ()
newPlayer controlHub renderHub ppos initState (spaceHandle, playerBody) = do

  say $ show ppos
  pST <- liftIO $ newMVar initState

  self <- getSelfPid

  send controlHub (ppos, self)

  physicsTimerId <- physicsTimer
  renderTimerId <- renderTimer
  send physicsTimerId self
  send renderTimerId self

  forever $ receiveWait
    [ match $ playerUpdateHandler playerBody pST
    , match $ playerRenderHandler renderHub ppos playerBody
    , match $ playerCommandHandler pST
    , match $ playerStateTransHandler playerBody ppos spaceHandle
    ]

playerUpdateHandler :: Body -> MVar PlayerState -> PhysicsUpdate -> Process ()
playerUpdateHandler b pST _ = liftIO $ do
  p <- readMVar pST
  let t = 0.01
  airResist b t
  traverse_ (engine b t) $ p ^. movingState

playerRenderHandler :: ProcessId -> PlayerPosition -> Body -> GameRender -> Process ()
playerRenderHandler renderHub ppos b _ = do
  pos <- liftIO $ getPosition b
  send renderHub $ PlayerRender ppos pos

playerCommandHandler :: MVar PlayerState -> Command -> Process ()
playerCommandHandler pST c = do
  say $ show c
  s <- liftIO $ modifyMVar pST (pure . stateTrans c)
  self <- getSelfPid
  traverse_ (\st -> send self st) s

playerStateTransHandler :: Body -> PlayerPosition -> ProcessId -> StateTrans -> Process ()
playerStateTransHandler b ppos spaceHandle st = do
  say $ show st
  pos <- liftIO $ getPosition b
  let
    target = case ppos of
      Player1 -> Player2
      Player2 -> Player1
  send spaceHandle $ CreateBullet pos target

stateTrans :: Command -> PlayerState -> (PlayerState, [StateTrans])
stateTrans c pState =
  let
    (pm, sp) = pState ^. movingState & movingChange c
    (pf, sf) = pState ^. fireState & fireChange c
  in
    (pState & movingState .~ pm & fireState .~ pf, catMaybes [sp, sf])

fireChange :: Command -> ([Card], [Card]) -> (([Card], [Card]), Maybe StateTrans)
fireChange c fs =
  case c of
    Command MoveLeft Press -> (goLeft fs, Nothing)
    Command MoveRight Press -> (goRight fs, Nothing)
    Command MoveUp Press -> launchMiddle fs
    _ -> (fs, Nothing)
  where
    goRight (l, r : rs) = (r : l, rs)
    goRight (l, []) = (l, [])
    goLeft (l : ls, rs) = (ls, l : rs)
    goLeft ([], r) = ([], r)
    launchMiddle (l, r : rs) = ((l, rs), Just $ Fire r)
    launchMiddle (l, []) = ((l, []), Nothing)

movingChange :: Command -> [Buff] -> ([Buff], Maybe StateTrans)
movingChange c buffs =
  case commandState c of
    Press -> (if notElem newBuff buffs then newBuff : buffs else buffs, Nothing)
    Release -> (if elem newBuff buffs then delete newBuff buffs else buffs, Nothing)
  where
    newBuff = case commandType c of
      MoveUp -> MovingUp
      MoveDown -> MovingDown
      MoveLeft -> MovingLeft
      MoveRight -> MovingRight
