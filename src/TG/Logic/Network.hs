{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE OverloadedStrings #-}

module TG.Logic.Network where

import Control.Monad
import Data.Monoid
import Data.Foldable

import qualified Data.ByteString.Lazy as BL
import qualified Data.Serialize as S

import Control.Concurrent (threadDelay, forkIO)
import qualified Control.Concurrent.Chan as C

import Network.Wai (Application, responseLBS)
import Network.Wai.Handler.Warp
import Network.Wai.Handler.WarpTLS
import Network.Wai.Handler.WebSockets
import Network.WebSockets

import Network.HTTP.Types (status400)

import TG.Types
import TG.Logic.Init

server1 :: IO ()
server1 = setServer >>= localServer

setServer :: IO SS
setServer = do
  s :: SS <- C.newChan
  -- forkIO $ runTLS mytls mySettings $ app s
  forkIO $ run 888 $ app s
  return s

mySettings :: Settings
mySettings = setPort 443 defaultSettings

mytls :: TLSSettings
mytls = tlsSettingsChain certPath [chainPath, fullChainPath] keyPath where
  store = "/etc/letsencrypt/live/fff.prpr.link/"
  certPath = store <> "cert.pem"
  keyPath = store <> "privkey.pem"
  chainPath = store <> "chain.pem"
  fullChainPath = store <> "fullchain.pem"

app :: SS -> Application
app s = websocketsOr defaultConnectionOptions wsApp backupApp
  where
    wsApp :: ServerApp
    wsApp pending_conn = do
      print $ pendingRequest pending_conn
      case checkUserInfo $ requestHeaders $ pendingRequest pending_conn of
        Left e ->
          print e
        Right (UserInfo uname _) -> do
          conn <- acceptRequest pending_conn
          C.writeChan s (uname, conn)
          forever $ threadDelay 10000

    backupApp :: Application
    backupApp _ respond = respond $ responseLBS status400 [] ("Not a WebSocket request" :: BL.ByteString)

checkUserInfo :: Headers -> Either String UserInfo
checkUserInfo hs =
  case find ((== "User-Info") . fst) hs of
    Nothing -> Left "User Info not found"
    Just h -> S.decode $ snd h
