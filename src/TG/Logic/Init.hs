{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE OverloadedStrings #-}

module TG.Logic.Init where

import Control.Monad
import Data.Monoid
import qualified Data.Map as M

import Linear
import Control.Concurrent.MVar
import qualified Control.Concurrent.Chan as C
import qualified Network.WebSockets as WS

import Network.Transport.TCP (createTransport, defaultTCPParameters)
import Control.Distributed.Process
import Control.Distributed.Process.Node

import TG.Types

import TG.Logic.Misc
import TG.Logic.Space

localServer :: SS -> IO ()
localServer s = do
  Right t <- createTransport "127.0.0.1" "10501" defaultTCPParameters
  node <- newLocalNode t initRemoteTable
  runProcess node $ do
    mainId <- spawnLocal $ mainLogic
    spawnLocal $ ioManager s mainId
    longSleep

ioManager :: SS -> ProcessId -> Process ()
ioManager s mainId = void $ forever $ do
  (uname, conn) <- liftIO $ C.readChan s
  say $ "gotuser" <> uname
  playerControl <- spawnLocal $ newPlayerControl conn
  playerRender <- spawnLocal $ newPlayerRender conn
  send mainId (uname, playerControl, playerRender)

newPlayerControl :: WS.Connection -> Process ()
newPlayerControl conn = void $ do
  player :: ProcessId <- expect
  forever $ do
    ii :: Command <- liftIO $ WS.receiveData conn
    send player ii

newPlayerRender :: WS.Connection -> Process ()
newPlayerRender conn = void $ do
  forever $ do
    r :: DisplayData <- expect
    -- liftIO $ print $ "send" <> show r
    liftIO $ WS.sendBinaryData conn r

newRenderHub :: ProcessId -> ProcessId -> Process ()
newRenderHub p1Render p2Render = do
  self <- getSelfPid
  renderTimerId <- renderTimer
  send renderTimerId self

  renderIndex <- liftIO $ newMVar (0, 0, M.empty)
  displayData <- liftIO $ newMVar (0,0,[])

  forever $ receiveWait
    [ match $ hubDataHandler renderIndex displayData
    , match $ hubRenderHandler p1Render p2Render displayData
    ]

hubDataHandler :: MVar (V2 Double, V2 Double, M.Map ProcessId (V2 Double)) -> MVar DisplayData -> RenderData -> Process ()
hubDataHandler renderIndex displayData rr = void $ liftIO $ do
  -- r <- takeMVar renderIndex
  -- putMVar renderIndex (composeData r)
  -- p <- tryPutMVar displayData (procData $ composeData r)
  modifyMVarMasked_ renderIndex modifyData
  where
    composeData (r1, r2, bullets) =
      case rr of
        PlayerRender Player1 d1 -> (d1, r2, bullets)
        PlayerRender Player2 d2 -> (r1, d2, bullets)
        BulletRender b bPos -> (r1, r2, M.insert b bPos bullets)
    procData (r1, r2, bullets) = (r1, r2, M.elems bullets)
    modifyData x = do
      let xx = composeData x
      tryPutMVar displayData (procData $ xx)
      return xx


hubRenderHandler :: ProcessId -> ProcessId -> MVar DisplayData -> GameRender -> Process ()
hubRenderHandler p1Render p2Render displayData _ = do
  r <- liftIO $ tryTakeMVar displayData
  case r of
    Just rr -> do
      send p1Render rr
      send p2Render rr
    Nothing -> do
      say "take fail"
      return ()


newControlHub :: ProcessId -> ProcessId -> Process ()
newControlHub p1Ctrl p2Ctrl = forever $ do
  (ppos, p) :: (PlayerPosition, ProcessId) <- expect
  case ppos of
    Player1 -> send p1Ctrl p
    Player2 -> send p2Ctrl p

mainLogic :: Process ()
mainLogic = void $ do
  (uname1, pCtrl1, pRender1) :: (UserName, ProcessId, ProcessId) <- expect
  (uname2, pCtrl2, pRender2) :: (UserName, ProcessId, ProcessId) <- expect

  renderHub <- spawnLocal $ newRenderHub pRender1 pRender2
  controlHub <- spawnLocal $ newControlHub pCtrl1 pCtrl2

  spaceHandle <- spawnLocal $ newGameSpace renderHub controlHub

  let initPlayerState = PlayerState [] (replicate 5 CardFireBall, replicate 3 CardIceBall)
  send spaceHandle $ CreatePlayer Player1 initPlayerState
  send spaceHandle $ CreatePlayer Player2 initPlayerState
