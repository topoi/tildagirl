{-# LANGUAGE ScopedTypeVariables #-}
module TG.Logic.Misc where

import Control.Monad

import Control.Distributed.Process
import Control.Distributed.Process.Extras.Timer
import Control.Distributed.Process.Extras.Time

import TG.Logic.Physics
import TG.Types

physicsTimer :: Process ProcessId
physicsTimer = spawnLocal $ void $ do
  pid :: ProcessId <- expect
  periodically (milliSeconds 10) (send pid PhysicsUpdate)

renderTimer :: Process ProcessId
renderTimer = spawnLocal $ void $ do
  pid :: ProcessId <- expect
  periodically (milliSeconds 17) (send pid GameRender)

longSleep :: Process ()
longSleep = sleep (hours 300)
