{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TemplateHaskell #-}
module TG.Types where

import GHC.Generics
import Data.Serialize
import Data.Binary

import Linear

import Control.Lens

import Network.WebSockets

import Physics.Hipmunk

import Control.Concurrent.Chan
import Control.Distributed.Process
import Data.Map


type SS = Chan (UserName, Connection)

type UserName = String
data UserInfo = UserInfo UserName String
  deriving (Eq, Show, Generic)
instance Binary UserInfo
instance Serialize UserInfo

---------------------


data Buff
  = MovingUp
  | MovingDown
  | MovingLeft
  | MovingRight
  deriving (Eq, Show, Generic)
instance Binary Buff

data Card
  = CardIceBall
  | CardFireBall
  deriving (Eq, Show, Generic)
instance Binary Card

data PlayerState = PlayerState
  { _movingState :: [Buff]
  , _fireState :: ([Card],[Card])
  }
  deriving (Eq, Show, Generic)
instance Binary PlayerState

makeLenses ''PlayerState

data StateTrans = Fire Card
  deriving (Eq, Show, Generic)
instance Binary StateTrans

--------------
data PlayerPosition = Player1 | Player2
  deriving (Eq, Ord, Show, Generic)
instance Binary PlayerPosition
instance Serialize PlayerPosition

---
data PhysicsUpdate = PhysicsUpdate
  deriving (Eq, Show, Generic)
instance Binary PhysicsUpdate

data GameRender = GameRender
  deriving (Eq, Show, Generic)
instance Binary GameRender

data RenderData
  = PlayerRender PlayerPosition (V2 Double)
  | BulletRender ProcessId (V2 Double)
  deriving (Eq, Show, Generic)
instance Binary RenderData


type DisplayData = (V2 Double, V2 Double, [V2 Double])
---------------------
type ControlInfo = Map PlayerPosition ProcessId
data CreateConfig
  = CreatePlayer PlayerPosition PlayerState
  | CreateBullet (V2 Double) PlayerPosition
  deriving (Eq, Show, Generic)
instance Binary CreateConfig
----

data CommandType = MoveLeft | MoveRight | MoveUp | MoveDown
  deriving (Eq, Show, Generic)
instance Binary CommandType
instance Serialize CommandType

data CommandState = Press | Release
  deriving (Eq, Show, Generic)
instance Binary CommandState
instance Serialize CommandState

data Command
  = Command
  { commandType :: CommandType
  , commandState :: CommandState
  }
  deriving (Eq, Show, Generic)
instance Binary Command
instance Serialize Command

instance (Serialize a1, Serialize a2, Serialize a3) => WebSocketsData (a1, a2, a3) where
  -- fromDataMessage (Text b _) = d
  --   where Right d = decode b

  -- fromDataMessage (Binary b) = d
  --   where Right d = decode b

  fromLazyByteString b = d
    where Right d = decodeLazy b

  toLazyByteString = encodeLazy

instance (Serialize a1, Serialize a2) => WebSocketsData (a1, a2) where
  -- fromDataMessage (Text b _) = d
  --   where Right d = decode b

  -- fromDataMessage (Binary b) = d
  --   where Right d = decode b

  fromLazyByteString b = d
    where Right d = decodeLazy b

  toLazyByteString = encodeLazy

instance Serialize a => WebSocketsData (V2 a) where
  -- fromDataMessage (Text b _) = d
  --   where Right d = decode b

  -- fromDataMessage (Binary b) = d
  --   where Right d = decode b

  fromLazyByteString b = d
    where Right d = decodeLazy b

  toLazyByteString = encodeLazy

instance WebSocketsData Command where
  -- fromDataMessage (Text b _) = d
  --   where Right d = decode b

  -- fromDataMessage (Binary b) = d
  --   where Right d = decode b


  fromLazyByteString b = d
    where Right d = decodeLazy b

  toLazyByteString = encodeLazy
