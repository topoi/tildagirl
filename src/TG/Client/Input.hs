module TG.Client.Input where

import Control.Monad

import Control.Concurrent
import Graphics.GPipe
import qualified Graphics.GPipe.Context.GLFW as GLFW
import TG.Types

inputDevice :: Window os c ds -> Chan Command -> ContextT GLFW.Handle os IO ()
inputDevice w inputChan = void $
  GLFW.setKeyCallback w (Just $ getInput inputChan)

getInput i k _ st _ =
  case (st, commandMap k) of
    (GLFW.KeyState'Pressed, Just cmd)  -> writeChan i $ Command cmd Press
    (GLFW.KeyState'Released, Just cmd) -> writeChan i $ Command cmd Release
    _ -> return ()

commandMap k =
  case k of
    GLFW.Key'W ->
      Just MoveUp
    GLFW.Key'A ->
      Just MoveLeft
    GLFW.Key'S ->
      Just MoveDown
    GLFW.Key'D ->
      Just MoveRight
    _ -> Nothing
