{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE OverloadedStrings #-}

module TG.Client.Network where

import Control.Monad
import Control.Monad.Trans.State.Lazy
import Control.Monad.Trans.Class

import qualified Data.Serialize as S

import Control.Concurrent

import Network.WebSockets
import Wuss

import Linear

import TG.Types
import TG.Client.Init

client1 :: IO ()
client1 = setClient >>= renderMain

setClient :: IO (Chan Command, MVar DisplayData)
setClient = do
  i :: Chan Command <- newChan
  r :: MVar DisplayData <- newMVar (0,0,[])
  -- forkIO $ runSecureClientWith "fff.prpr.link" 443 "/" defaultConnectionOptions (userInfoHeader user1) $ comunicate i r
  forkIO $ runClientWith "127.0.0.1" 888 "/" defaultConnectionOptions (userInfoHeader user1) $ comunicate i r
  return (i, r)


userInfoHeader :: UserInfo -> Headers
userInfoHeader u = [("User-Info", S.encode $ u)]

user1 = UserInfo "haha1" "233"

comunicate :: Chan Command -> MVar DisplayData -> Connection -> IO ()
comunicate i r conn = do
  forkIO $ forever $ do
    rr :: DisplayData <- receiveData conn
    void $ tryPutMVar r rr

  forkIO $ forever $ do
    ii <- readChan i
    sendBinaryData conn ii
  forkPingThread conn 30
  forever $ threadDelay 3000
