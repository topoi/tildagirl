module TG.Client.Init where

import Control.Monad

import Control.Concurrent
import Graphics.GPipe
import qualified Graphics.GPipe.Context.GLFW as GLFW
import TG.Types
import TG.Client.Input
import TG.Client.Render

renderMain :: (Chan Command, MVar DisplayData) -> IO ()
renderMain (i, r) = void $ do
  runContextT GLFW.defaultHandleConfig $ do
    win <- newWindow (WindowFormatColor RGBA8) windowConfig
    inputDevice win i
    renderDevice win r

windowConfig :: GLFW.WindowConfig
windowConfig = GLFW.WindowConfig 500 500 "Hola" Nothing [] Nothing
