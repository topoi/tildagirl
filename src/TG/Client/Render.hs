{-# LANGUAGE ScopedTypeVariables, TypeFamilies #-}

module TG.Client.Render where

import Control.Monad
import Data.Monoid
import Control.Monad.Trans.Class (lift)
import Data.Foldable

import qualified Data.Serialize as S

import Control.Concurrent

import Graphics.GPipe
import qualified Graphics.GPipe.Context.GLFW as GLFW

import Data.Word
import qualified Data.Vector.Storable as S
import Codec.Picture
import Data.List.Split
import Data.Bits

import TG.Types
import TG.Client.Render.Resource
import TG.Client.Render.Shader

bkData =
  [ (V4 (-1) 1 0 1, V2 0 1 )
  , (V4 1 1 0 1, V2 1 1 )
  , (V4 1 (-1) 0 1, V2 1 0 )
  , (V4 (-1) (-1) 0 1, V2 0 0 )
  ]

playerData = zip
  ((V2 (V2 2 0) (V2 0 8) !*) <$> [ V2 (-1) 1 , V2 1 1 , V2 1 (-1) , V2 (-1) (-1) ])
  [V2 1 0, V2 0 0, V2 0 1, V2 1 1]

bulletData =
  (\d -> (V2 (sin d / 2) (cos d))) <$> (\i -> fromIntegral i * 2 * pi / 100) <$> [0..100]

renderDevice :: ContextHandler ctx => Window os RGBAFloat ds -> MVar DisplayData -> ContextT ctx os IO b
renderDevice win r = do
  bkVB :: Buffer os (B4 Float, B2 Float) <- newBuffer 4
  writeBuffer bkVB 0 bkData

  pVB :: Buffer os (B2 Float, B2 Float) <- newBuffer 4
  writeBuffer pVB 0 playerData

  bVB :: Buffer os (B2 Float) <- newBuffer 100
  writeBuffer bVB 0 bulletData

  player1Buffer :: Buffer os (Uniform (B2 Float)) <- newBuffer 1
  player2Buffer :: Buffer os (Uniform (B2 Float)) <- newBuffer 1
  bulletBuffer :: Buffer os (Uniform (B2 Float)) <- newBuffer 1

  let Image w h ds = girlPic
  tex <- newTexture2D RGBA8 (V2 w h) 1
  let
    dd :: [V4 Float]
    dd = fmap (\[r, g, b, a] -> V4 r g b a) $ chunksOf 4 $ fmap (\s -> fromIntegral s / fromIntegral (maxBound :: Word8)) $ S.toList ds
  writeTexture2D tex 0 0 (V2 w h) dd

  bkTex <- newTexture2D R8 (V2 8 8) 1
  let
    whiteBlack = cycle [minBound,maxBound] :: [Word32]
    blackWhite = tail whiteBlack
  writeTexture2D bkTex 0 0 (V2 8 8) (cycle (take 8 whiteBlack ++ take 8 blackWhite))

  bkShader <- compileShader $ backgroundShader bkTex win
  pShader <- compileShader $ playerShader player1Buffer player2Buffer tex win
  bShader <- compileShader $ bulletShader bulletBuffer win

  forever $ do
    (d1, d2, bs) :: DisplayData <- lift $ takeMVar r

    render $ do
      clearWindowColor win (V4 1 1 1 1)

    render $ do
      bkVA <- newVertexArray bkVB
      let bkPA = toPrimitiveArray TriangleFan bkVA
      bkShader bkPA

    writeBuffer player1Buffer 0 [(realToFrac :: Double -> Float) <$> d1]
    writeBuffer player2Buffer 0 [(realToFrac :: Double -> Float) <$> d2]
    render $ do
      pVA <- newVertexArray pVB
      let pPA = toPrimitiveArray TriangleFan pVA
      pShader pPA

    let
      renderBullet b = do
        writeBuffer bulletBuffer 0 [(realToFrac :: Double -> Float) <$> b]
        render $ do
          bVA <- newVertexArray bVB
          let bPA = toPrimitiveArray TriangleFan bVA
          bShader bPA
    traverse_ renderBullet bs

    swapWindowBuffers win
