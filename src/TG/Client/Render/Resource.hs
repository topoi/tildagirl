{-# LANGUAGE TemplateHaskell #-}

module TG.Client.Render.Resource where

import Data.FileEmbed
import Data.ByteString
import Codec.Picture

girlPic :: Image PixelRGBA8
girlPic = convertRGBA8 dPic
  where Right dPic = decodePng $(embedFile "res/chris.png")
