{-# LANGUAGE ScopedTypeVariables, TypeFamilies #-}
module TG.Client.Render.Shader where

import Data.Monoid
import Data.Word
import Graphics.GPipe

backgroundShader :: Texture2D os (Format RFloat) -> Window os RGBAFloat ds -> Shader os (PrimitiveArray Triangles (B4 Float, B2 Float)) ()
backgroundShader bkTex win = do
  primitiveStream :: PrimitiveStream Triangles (V4 VFloat, V2 VFloat) <- toPrimitiveStream id
  fragmentStream <- rasterize (const (FrontAndBack, ViewPort (V2 0 0) (V2 500 500), DepthRange 0 1)) primitiveStream
  let
    filter = SamplerFilter Nearest Nearest Nearest Nothing
    edge = (pure Repeat, undefined)
  samp <- newSampler2D (const (bkTex, filter, edge))
  let
    sampleTexture = (\s -> V4 s s s 1) . sample2D samp SampleAuto Nothing Nothing
    fragmentStream2 = fmap sampleTexture fragmentStream
  drawWindowColor (const (win, ContextColorOption NoBlending (pure True))) fragmentStream2


playerShader :: Buffer os (Uniform (B2 Float)) -> Buffer os (Uniform (B2 Float)) -> Texture2D os (Format RGBAFloat) -> Window os RGBAFloat ds -> Shader os (PrimitiveArray Triangles (B2 Float, B2 Float)) ()
playerShader player1Buffer player2Buffer tex win = do
  primitiveStream <- toPrimitiveStream id
  (V2 p1x p1y) <- getUniform (const (player1Buffer,0))
  (V2 p2x p2y) <- getUniform (const (player2Buffer,0))
  let
    primitiveStream1 :: PrimitiveStream Triangles (V4 VFloat, V2 VFloat)
    primitiveStream1 = (\(pos,uv) -> (transMatrix p1x p1y !* toV4 pos, uv)) <$> primitiveStream
    primitiveStream2 :: PrimitiveStream Triangles (V4 VFloat, V2 VFloat)
    primitiveStream2 = (\(pos, uv) -> (transMatrix p2x p2y !* toV4 pos, uv)) <$> primitiveStream
    primitiveStreamAll = primitiveStream1 <> primitiveStream2
  fragmentStream <- rasterize (const (FrontAndBack, ViewPort (V2 0 0) (V2 500 500), DepthRange 0 1)) primitiveStreamAll
  let
    filter = SamplerFilter Nearest Nearest Nearest Nothing
    edge = (pure Repeat, undefined)
  samp <- newSampler2D (const (tex, filter, edge))
  let
    sampleTexture = sample2D samp SampleAuto Nothing Nothing
    fragmentStream2 = fmap sampleTexture fragmentStream
  drawWindowColor (const (win, ContextColorOption simpleBlender (pure True))) fragmentStream2

bulletShader :: Buffer os (Uniform (B2 Float)) -> Window os RGBAFloat ds -> Shader os (PrimitiveArray Triangles (B2 Float)) ()
bulletShader bulletBuffer win = do
  primitiveStream :: PrimitiveStream Triangles (V2 VFloat) <- toPrimitiveStream id
  (V2 bx by) <- getUniform (const (bulletBuffer,0))
  let
    primitiveStream1 :: PrimitiveStream Triangles (V4 VFloat, V4 VFloat)
    primitiveStream1 = (\pos -> (transMatrix bx by !* toV4 pos, V4 1 0 1 1)) <$> primitiveStream
  fragmentStream <- rasterize (const (FrontAndBack, ViewPort (V2 0 0) (V2 500 500), DepthRange 0 1)) primitiveStream1
  drawWindowColor (const (win, ContextColorOption simpleBlender (pure True))) fragmentStream


toV4 :: V2 VFloat -> V4 VFloat
toV4 (V2 x y) = V4 x y 0 1

transMatrix x y = scaleMatrix 0.02 !*! m1
  where
    m1 = V4
      (V4 1 0 0 (x))
      (V4 0 1 0 (y))
      (V4 0 0 1 0)
      (V4 0 0 0 1)

scaleMatrix d = V4
  (V4 d 0 0 0)
  (V4 0 d 0 0)
  (V4 0 0 1 0)
  (V4 0 0 0 1)

simpleBlender :: Blending
simpleBlender = BlendRgbAlpha (FuncAdd, FuncAdd) (BlendingFactors SrcAlpha OneMinusSrcAlpha, BlendingFactors One Zero) (V4 0 0 0 0)
